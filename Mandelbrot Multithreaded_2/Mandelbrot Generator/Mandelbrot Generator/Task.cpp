#include <iostream>
#include <thread>

#include "Task.h"
#include "Mandelbrot.h"

CTask::CTask()
	:m_ivalue(0)
{

}

CTask::CTask(int _value)
	: m_ivalue(_value)
{

}

CTask::~CTask()
{

}

void CTask::operator()() const
{
	//Sleep to simulate work being done
	//std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 101));
	CMandelBrot* mandelbrot = CMandelBrot::GetInstance();
	mandelbrot->Draw(backbuffer, startX, startY, width, height);
}

int CTask::getValue() const
{
	return m_ivalue;
}