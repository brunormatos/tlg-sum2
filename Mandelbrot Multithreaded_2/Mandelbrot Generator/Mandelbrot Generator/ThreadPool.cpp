//Library Includes
#include <iostream>
#include <fstream>			// ifstream
#include <string>
#include <thread>
#include <functional>

//Local Includes
#include "WorkQueue.h"
#include "Task.h"

//This Include
#include "ThreadPool.h"

//Static Variables
ThreadPool* ThreadPool::s_pThreadPool = nullptr;

ThreadPool::ThreadPool()
{
	// LOAD number of available threads from configuration file
	std::string STRING;
	std::ifstream infile;
	infile.open("config.txt");
	while (!infile.eof()) // Gets all lines inside file.
	{
		std::getline(infile, STRING); // Saves the line in STRING.
									  //std::cout << STRING; // Prints our STRING.
	}
	infile.close();
	const int totalThreadCount = std::stoi(STRING.substr(STRING.find("=") + 1));
	//m_iNumberOfThreads = std::thread::hardware_concurrency();
	m_iNumberOfThreads = totalThreadCount;
}


ThreadPool::ThreadPool(unsigned int _size)
{
	//Create a pool of threads equal to specified size
	m_iNumberOfThreads = _size;
}

ThreadPool::~ThreadPool()
{
	m_bStop = true;
	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		m_workerThreads[i].join();
	}
	// Iterate over all Backbuffers and bit them together

	int backbuffersCount = m_vecCBackBuffers.size();
	HDC dest_hDC = m_pMainBackBuffer->GetBFDC();
	int width = m_pMainBackBuffer->GetWidth();
	int heigth = m_pMainBackBuffer->GetHeight();

	for (unsigned int i = 0; i < backbuffersCount; i++)
	{	
		HDC m_hDC = m_vecCBackBuffers[i]->GetBFDC();
		BitBlt(dest_hDC, 0, 0, width, heigth, m_hDC, 0, 0, SRCCOPY);
	}

	/*for (unsigned int i = 0; i < backbuffersCount; i++)
	{
		delete m_vecCBackBuffers[i];
	}*/
	m_pMainBackBuffer->Present();
	m_vecCBackBuffers.clear();
	delete m_pWorkQueue;
	m_pWorkQueue = 0;
}

ThreadPool& ThreadPool::GetInstance()
{
	if (s_pThreadPool == nullptr)
	{
		s_pThreadPool = new ThreadPool();
	}
	
	return (*s_pThreadPool);
}

ThreadPool& ThreadPool::GetInstance(unsigned int _size)
{
	if (s_pThreadPool == nullptr)
	{
		s_pThreadPool = new ThreadPool(_size);
	}
	
	return (*s_pThreadPool);
}

void ThreadPool::DestroyInstance()
{
	delete s_pThreadPool;
	s_pThreadPool = 0;
}

void ThreadPool::Initialize()
{
	//Create a new Work Queue
	m_pWorkQueue = new CWorkQueue<CTask>();
}

void ThreadPool::InitializeBackbufferPool(HWND _hwd, int _winWidth, int _winHeight)
{
	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		CBackBuffer * bb = new CBackBuffer();
		bb->Initialise(_hwd, _winWidth, _winHeight);
		m_vecCBackBuffers.push_back(bb);
	}
}


void ThreadPool::Start()
{
	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		m_workerThreads.push_back(std::thread(&ThreadPool::DoWork, this, i, i*100, i*100, 100, 100, m_pMainBackBuffer));
	}
}

void ThreadPool::Submit(CTask _fItem)
{
	m_pWorkQueue->push(_fItem);
}

void ThreadPool::Stop()
{
	m_bStop = true;
}


void ThreadPool::DoWork(int threadId, int _startX, int _startY, int _width, int _height, CBackBuffer* _backbuffer)
{
	//Entry point of  a thread.
	std::cout << std::endl << "Thread with id " << std::this_thread::get_id() << "starting........" << std::endl;
	while (!m_bStop)
	{
		CTask WorkItem;
		WorkItem.backbuffer = _backbuffer;
		WorkItem.startX = _startX;
		WorkItem.startY = _startY;
		WorkItem.width = _width;
		WorkItem.height = _height;

		//If there is an item in the queue to be processed; just take it off the q and process it
		//m_pWorkQueue->blocking_pop(WorkItem);
		if (m_pWorkQueue->nonblocking_pop(WorkItem))
		{
			/*std::cout << std::endl << "Thread with id " << std::this_thread::get_id() << " is working on item " << WorkItem.getValue() << " in the work queue" << std::endl;
			WorkItem();
			//Sleep to simulate work being done
			std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 101));
			std::cout << std::endl << "Thread with id " << std::this_thread::get_id() << " finished processing item " << WorkItem.getValue() << std::endl;*/
			m_aiItemsProcessed++;
		}
		//Else just continue back to the beginning of the while loop.
		else
		{
			continue;
		}
	}
}

std::atomic_int& ThreadPool::getItemsProcessed()
{
	return m_aiItemsProcessed;
}

