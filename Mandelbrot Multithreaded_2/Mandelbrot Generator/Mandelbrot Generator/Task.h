// A functor class

#ifndef __CTASK_H__
#define __CTASK_H__

#include <Windows.h>
#include "backbuffer.h"

class CTask
{
public:
	CTask();
	CTask(int _value);
	~CTask();
	void operator()() const;
	int getValue() const;

	CBackBuffer* backbuffer = nullptr;
	int startX = 0;
	int startY = 0;
	int width = 1;
	int height = 1;

private:
	int m_ivalue;
};

#endif



