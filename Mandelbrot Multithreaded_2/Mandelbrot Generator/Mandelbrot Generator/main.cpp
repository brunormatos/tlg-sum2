#include <Windows.h>
#include <vector>
#include <string>
#include <math.h>

#include <vector>

#include "Mandelbrot.h"		// madelbrot generator


#include <vld.h>	//uncomment this if wanting to use visual leak detector


#define WINDOW_CLASS_NAME L"Mandelbrot Generator"
const unsigned int _kuiWINDOWWIDTH = 1400;
const unsigned int _kuiWINDOWHEIGHT = 1020;

//Global Variables
HINSTANCE g_hInstance;
std::vector<CBackBuffer*> g_vecBackbuffers;
CBackBuffer* g_mainBackbuffer = nullptr;

HWND textField;


LRESULT CALLBACK WindowProc(HWND _hwnd, UINT _uiMsg, WPARAM _wparam, LPARAM _lparam)
{
	HDC hdcStatic = (HDC)_wparam;
	PAINTSTRUCT ps;
	static HDC _hWindowDC;	//this must be static

	int windowWidth = _kuiWINDOWWIDTH, windowHeight = _kuiWINDOWHEIGHT;
	int regionsCountX = 10;
	int regionsCountY = 10;
	int regionWidth = static_cast<int>(std::floor(static_cast<float>(windowWidth) / static_cast<float>(regionsCountX)));
	int regionHeight = static_cast<int>(std::floor(static_cast<float>(windowHeight) / static_cast<float>(regionsCountY)));
	int bBufferCount = 5;
	int totalRegionCount = (regionsCountX * regionsCountY);
	int regionsPerBuffer = totalRegionCount / bBufferCount;

	RECT rec;
	if (GetClientRect(_hwnd, &rec))
	{
		//save new client viewport dimensions for rescalling images
		windowWidth = rec.right - rec.left;
		windowHeight = rec.bottom - rec.top;
	}
	switch (_uiMsg)
	{
	case WM_CREATE:
	{
		g_mainBackbuffer = new CBackBuffer();
 		g_mainBackbuffer->Initialise(_hwnd, windowWidth, windowHeight);

		for (int i = 0 ; i < bBufferCount; i++)
		{
			CBackBuffer* backbuffer = new CBackBuffer();
			backbuffer->Initialise(_hwnd, windowWidth, windowHeight);
			g_vecBackbuffers.push_back(backbuffer);
		}

		CMandelBrot* mandelbrot = CMandelBrot::GetInstance();
		for (int j = 0; j < regionsCountY; j++)
		{
			for (int i = 0; i < regionsCountX; i++)
			{
				int k = i + j*regionsCountX;
				int bB = 0;
				if (k < regionsPerBuffer) bB = 0;
				else if (k >= regionsPerBuffer && k < regionsPerBuffer * 2) bB = 1;
				else if (k >= regionsPerBuffer * 2 && k < regionsPerBuffer * 3) bB = 2;
				else if (k >= regionsPerBuffer * 3 && k < regionsPerBuffer * 4) bB = 3;
				else if (k >= regionsPerBuffer * 4) bB = 4;
					
				mandelbrot->Draw(g_vecBackbuffers[bB], regionWidth*i, regionHeight * j, regionWidth, regionHeight);
			}
		}

		int bBuffersCount = g_vecBackbuffers.size();
		for (int i = 0; i < bBuffersCount; i++)
		{
			g_vecBackbuffers[i]->BlitIntoDC(g_mainBackbuffer->GetBFDC());
		}

		std::wstring strOut = L"Hello World!";
		textField = CreateWindowEx(NULL, L"Static", strOut.c_str(), WS_CHILD , 20, 20, 300, 20, _hwnd, NULL, NULL, NULL);

		HDC hdc = g_mainBackbuffer->GetBFDC();
		SelectObject(hdc, GetStockObject(DEFAULT_GUI_FONT));
		SetBkMode(hdc, TRANSPARENT);
		SetTextColor(hdc, RGB(255, 0, 0));
	}
	break;
	case WM_KEYDOWN:
	{
		switch (_wparam)
		{
		case 0x31:
			g_vecBackbuffers[0]->Present();
			break;
		case 0x32:
			g_vecBackbuffers[1]->Present();
			break;
		case 0x33:
			g_vecBackbuffers[2]->Present();
			break;
		case 0x34:
			g_vecBackbuffers[3]->Present();
			break;
		case 0x35:
			g_vecBackbuffers[4]->Present();
			break;
		case VK_RETURN:
			g_mainBackbuffer->Present();
			break;
		case VK_ESCAPE:
		{
			SendMessage(_hwnd, WM_CLOSE, 0, 0);
			return(0);
		}
		break;
		default:
			break;
		}
	}
	break;
	case WM_PAINT:
	{

		_hWindowDC = BeginPaint(_hwnd, &ps);

		g_mainBackbuffer->Present();

		EndPaint(_hwnd, &ps);
		return (0);
	}
	break;	
	case WM_CLOSE:
	{
		PostQuitMessage(0);
	}
	break;
	default:
		break;
	}
	return (DefWindowProc(_hwnd, _uiMsg, _wparam, _lparam));
}


HWND CreateAndRegisterWindow(HINSTANCE _hInstance)
{
	WNDCLASSEX winclass; // This will hold the class we create.
	HWND hwnd;           // Generic window handle.

						 // First fill in the window class structure.
	winclass.cbSize = sizeof(WNDCLASSEX);
	winclass.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc = WindowProc;
	winclass.cbClsExtra = 0;
	winclass.cbWndExtra = 0;
	winclass.hInstance = _hInstance;
	winclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	winclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winclass.hbrBackground =
		static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	winclass.lpszMenuName = NULL;
	winclass.lpszClassName = WINDOW_CLASS_NAME;
	winclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register the window class
	if (!RegisterClassEx(&winclass))
	{
		return (0);
	}

	// create the window
	hwnd = CreateWindowEx(NULL, // Extended style.
		WINDOW_CLASS_NAME,      // Class.
		L"Mandelbrot Generator",   // Title.
		WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		10, 10,                    // Initial x,y.
		_kuiWINDOWWIDTH, _kuiWINDOWHEIGHT,                // Initial width, height.
		NULL,                   // Handle to parent.
		NULL,                   // Handle to menu.
		_hInstance,             // Instance of this application.
		NULL);                  // Extra creation parameters.

	return hwnd;
}



int WINAPI WinMain(HINSTANCE _hInstance,
	HINSTANCE _hPrevInstance,
	LPSTR _lpCmdLine,
	int _nCmdShow)
{
	MSG msg;  //Generic Message

	HWND _hwnd = CreateAndRegisterWindow(_hInstance);

	if (!(_hwnd))
	{
		return (0);
	}

	CMandelBrot* mandelbrot = CMandelBrot::GetInstance();

	// Enter main event loop
	while (true)
	{
		// Test if there is a message in queue, if so get it.
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// Test if this is a quit.
			if (msg.message == WM_QUIT)
			{
				break;
			}

			// Translate any accelerator keys.
			TranslateMessage(&msg);
			// Send the message to the window proc.
			DispatchMessage(&msg);
		}

	}

	//delete mandelbrot;
	delete mandelbrot;
	delete g_mainBackbuffer;
	int bBufferCount = g_vecBackbuffers.size();
	for (int i = 0; i < bBufferCount; i++)
	{
		delete g_vecBackbuffers[i];
	}
	g_vecBackbuffers.clear();
	// Return to Windows like this...
	return (static_cast<int>(msg.wParam));
}
