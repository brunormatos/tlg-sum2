#ifndef __MANDELBROT_H__
#define __MANDELBROT_H__

#include <Windows.h>
#include "backbuffer.h"

#define MAXITERATIONS 255

typedef struct tagComplexNumber
{
	float _fReal;
	float _fImag;
} ComplexNumber;

class CMandelBrot
{
public:
	~CMandelBrot();
	static CMandelBrot* GetInstance();
	void Draw(HWND);
	void Draw(CBackBuffer*);
	void Draw(CBackBuffer*, int, int, int, int);
private:
	CMandelBrot();

	// Singleton Instance
	static CMandelBrot* s_pMandelBrotInstance;
};

#endif