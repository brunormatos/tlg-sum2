/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: Task.h
Description	: The CTask class prototype
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#pragma once

#ifndef __CTASK_H__
#define __CTASK_H__

#include <Windows.h>
#include "backbuffer.h"

class CTask
{
public:
	/***********************
	* CTask(): the class default constructor
	* @author: Bruno Matos
	* @arg: no arguments
	* @return: CTask
	********************/
	CTask();
	/***********************
	* CTask: another class constructor designed to pass each backbuffer's region's dimensions
	* @author: Bruno Matos
	* @arg: int			|  the region's starting X coordinate
	* @arg: int			|  the region's starting y coordinate|
	* @arg: int			|	
	* @arg: int			|
	* @arg: int			|
	* @arg: int			|
	* @return: CTask
	********************/
	CTask(int, int, int, int, int, int);

	/***********************
	* The class destructor: destroys the current instance
	* @author: Bruno Matos
	********************/
	~CTask();
	void operator()(HDC) const;
private:
	//The current region's top left corner X pixel coordinate
	int m_iStartX;
	//The current region's top left corner Y pixel coordinate
	int m_iStartY;
	//The region's width
	int m_iWidth;
	//The region's height
	int m_iHeight;
	//One of the two fractal's scalable coefficients
	int m_iFractWidth;
	//One of the two fractal's scalable coefficients
	int m_iFractHeight;
};

#endif



