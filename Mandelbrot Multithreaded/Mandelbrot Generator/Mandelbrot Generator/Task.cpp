/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: Task.cpp
Description	: The CTask class definition
Author		: Bruno Matos (based on template provided by lecturer Asma Shakil)
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#include "Task.h"
#include "Mandelbrot.h"

CTask::CTask()
	: m_iStartX(0),
	m_iStartY(0),
	m_iWidth(0),
	m_iHeight(0),
	m_iFractWidth(0),
	m_iFractHeight(0)
{

}

CTask::CTask(int _startX, int _startY, int _regionWidth, int _regionHeight, int _fractWidth, int _fractHeight)
	: m_iStartX(_startX),
	m_iStartY(_startY),
	m_iWidth(_regionWidth),
	m_iHeight(_regionHeight),
	m_iFractWidth(_fractWidth),
	m_iFractHeight(_fractHeight)
{

}

CTask::~CTask()
{

}

void CTask::operator()(HDC _hdc) const
{
	CMandelBrot* mandelbrot = CMandelBrot::GetInstance();
	mandelbrot->Draw(_hdc, m_iStartX, m_iStartY, m_iWidth, m_iHeight, m_iFractWidth, m_iFractHeight);
}
