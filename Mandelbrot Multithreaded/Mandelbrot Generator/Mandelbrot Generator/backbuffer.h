/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: backbuffer.h
Description	: a class prototype for backbuffers
Author		: Bruno Matos (based on a template provided by lecturer Asma Shakil)
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#pragma once

#ifndef __BACKBUFFER_H__
#define __BACKBUFFER_H__

// Library Includes
#include <Windows.h>

// Local Includes

// Types

// Constants

// Prototypes
class CBackBuffer
{
    // Member Functions
public:
    CBackBuffer();
    ~CBackBuffer();


    /***********************
    * bool Initialise: attempts to initialize a backbuffer with a device context
    * @author: Asma Shakil
	* @arg:	HWND	| a window handler
	* @arg:	int		| the width pixel dimension for creating a the backbuffer area
	* @arg:	int		| the height pixel dimension for creating a the backbuffer area
    * @return: bool
    ********************/
    bool Initialise(HWND _hWnd, int _iWidth, int _iHeight);

	/***********************
	* GetBFDC: gets the backbuffer device context
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: HDC
	********************/
    HDC GetBFDC() const;

	/***********************
	* GetHeight: gets the backbuffer height pixel dimension
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: int
	********************/
    int GetHeight() const;

	/***********************
	* GetWidth: gets the backbuffer width pixel dimension
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: int
	********************/
    int GetWidth() const;

	/***********************
	* Clear: clears the backbuffer canvas
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: void
	********************/
    void Clear();

	/***********************
	* Present: copies the backbuffer device context contents into the window handler device context (which displays it on screen)
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: void
	********************/
    void Present();

	/***********************
	* Present: copies the backbuffer device context contents into another device context
	* @author: Bruno Matos
	* @arg:	HDC		| the target device context
	* @return: void
	********************/
	void BlitIntoDC(HDC);

protected:

private:
    CBackBuffer(const CBackBuffer& _kr);
    CBackBuffer& operator= (const CBackBuffer& _kr);

    // Member Variables
public:

protected:
	//A window handler
    HWND m_hWnd;
	//The current backbuffer device context
    HDC m_hDC;
	//Bitmaps for saving the current backbuffer pixel information
    HBITMAP m_hSurface;
    HBITMAP m_hOldObject;
	//The current backbuffers width and height dimensions
    int m_iWidth;
    int m_iHeight;

private:

};

#endif    // __BACKBUFFER_H__

