/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: WorkQueue.h
Description	: A simple work queue template
Author		: class template provided by lecturer Asma Shakil
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#pragma once

#ifndef __WORKQUEUE_H__
#define __WORKQUEUE_H__

#include <queue>
#include <mutex>


template<typename T>
class CWorkQueue
{
public:
	CWorkQueue() {}

	/***********************
	* push: Insert an item at the back of the queue and signal any thread that might be waiting for the q to be populated
	* @author: Asma Shakil
	* @arg: const T&		| the item to be processed
	* @return: void
	********************/
	void push(const T& item)
	{
		std::lock_guard<std::mutex> _lock(m_WorkQMutex);
		workQ.push(std::move(item));
		m_WorkQCondition.notify_one();
	}

	/***********************
	* nonblocking_pop: attempts to get a workitem from the queue. If the Q is empty returns false.
	* @author: Asma Shakil
	* @arg: T&			| the item to be processed
	* @return: bool
	********************/
	bool nonblocking_pop(T& _workItem)
	{
		std::lock_guard<std::mutex> _lock(m_WorkQMutex);
		//If the queue is empty return false
		if (workQ.empty())
		{
			return false;
		}
		_workItem = std::move(workQ.front());
		workQ.pop();
		return true;
	}

	/***********************
	* blocking_pop: attempts to get a workitem from the queue. If the Q is empty blocks the thread until a notification is sent to it.
	* @author: Asma Shakil
	* @arg: T&			| the item to be processed
	* @return: void
	********************/
	void blocking_pop(T& _workItem)
	{
		std::unique_lock<std::mutex> _lock(m_WorkQMutex);
		//If the queue is empty block the thread from running until a work item becomes available
		m_WorkQCondition.wait(_lock, [this] {return !workQ.empty(); });
		_workItem = std::move(workQ.front());
		workQ.pop();
	}

	/***********************
	* empty: Checking if the queue is empty or not
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: bool
	********************/
	bool empty() const
	{
		std::lock_guard<std::mutex> _lock(m_WorkQMutex);
		return workQ.empty();
	}

	
private:
	//A queue to store the items
	std::queue<T> workQ;
	//A mutex for handling blocking pops in a multi-threaded system
	mutable std::mutex m_WorkQMutex;
	//A condition variable for handling blocking pops in a multi-threaded system
	std::condition_variable m_WorkQCondition;

};
#endif
