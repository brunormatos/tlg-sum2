/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: ThreadPool.cpp
Description	: Thread pool class definition
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/

//Library Includes
#include <thread>
#include <functional>
#include "string"

//Local Includes
#include "WorkQueue.h"
#include "Task.h"

//This Include
#include "ThreadPool.h"

//Static Variables
ThreadPool*
ThreadPool::s_pThreadPool = nullptr;

ThreadPool::ThreadPool()
{
	m_iNumberOfThreads = std::thread::hardware_concurrency();
	m_pMainBackBuffer = new CBackBuffer();
}


ThreadPool::~ThreadPool()
{
	m_bStop = true;
	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		m_workerThreads[i].join();
	}

	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		delete m_vecBackBuffers[i];
	}
	
	m_vecBackBuffers.clear();
	delete m_pMainBackBuffer;
	delete m_pWorkQueue;
	m_pWorkQueue = 0;
}

ThreadPool&
ThreadPool::GetInstance()
{
	if (s_pThreadPool == nullptr)
	{
		s_pThreadPool = new ThreadPool();
	}
	
	return (*s_pThreadPool);
}

void
ThreadPool::DestroyInstance()
{
	delete s_pThreadPool;
	s_pThreadPool = 0;
}

void
ThreadPool::Initialize()
{
	//Create a new Work Queue
	m_pWorkQueue = new CWorkQueue<CTask>();
}

void
ThreadPool::Start()
{
	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		m_workerThreads.push_back(std::thread(&ThreadPool::DoWork, this, i));
	}
}

void
ThreadPool::Submit(CTask _fItem)
{
	m_pWorkQueue->push(_fItem);
}

void
ThreadPool::Stop()
{
	m_bStop = true;

	for (unsigned int i = 0; i < m_iNumberOfThreads; i++)
	{
		//copy each backbuffer device context image into the main backbuffer device context
		m_vecBackBuffers[i]->BlitIntoDC(m_pMainBackBuffer->GetBFDC());
	}

	int loadTime = 0;
	//As the loading process has finished we must stop the clock and read the measured time interval.
	if (m_pClock != nullptr)
	{
		m_pClock->Process();
		loadTime = static_cast<int>(m_pClock->GetDeltaTick());
	}
	

	//witting on-screen debug messages with information about the project settings
	RECT rect;
	GetClientRect(NULL, &rect);
	rect.left = 40;
	rect.top = 10;
	HDC hdc = m_pMainBackBuffer->GetBFDC();
	SelectObject(hdc, GetStockObject(DEFAULT_GUI_FONT));
	SetBkMode(hdc, TRANSPARENT);
	SetTextColor(hdc, RGB(0, 0, 0));
	std::wstring strOut = L"Thread pool using " + std::to_wstring(m_iNumberOfThreads) + L" threads." +
		L"\nNumber of regions: " + std::to_wstring(getItemsProcessed()) + L"."+
		L"\nTotal load time: " + std::to_wstring(loadTime) + L" milliseconds." +
		L"\nPress TAB KEY for cycling through each thread work load or RETURN to display complete drawing.\n";
	std::wstring strOutComplete = strOut + L"Displaying complete performed work.";

	//Draws mainBackbuffer's specific text information into the HDC of the mainBackBuffer
	DrawText(hdc, strOutComplete.c_str(), strOutComplete.length(), &rect, DT_NOCLIP);

	for (int i = 0; i < m_iNumberOfThreads; i++)
	{
		HDC hdcI = m_vecBackBuffers[i]->GetBFDC();
		m_vecBackBuffers[i]->BlitIntoDC(hdcI);
		SelectObject(hdcI, GetStockObject(DEFAULT_GUI_FONT));
		SetBkMode(hdcI, TRANSPARENT);
		SetTextColor(hdcI, RGB(0, 0, 0));
		std::wstring strThread = strOut + L"Displaying THREAD " + std::to_wstring(i) + L" performed work.";

		//Draws each backbuffer's specific text information into the HDC of the mainBackBuffer
		DrawText(hdcI, strThread.c_str(), strThread.length(), &rect, DT_NOCLIP);
	}

	m_pMainBackBuffer->Present();
}


void
ThreadPool::DoWork(unsigned int i)
{
	//Entry point of  a thread.
	while (!m_bStop)
	{
		CTask WorkItem;
		if (m_pWorkQueue->nonblocking_pop(WorkItem))
		{
			CBackBuffer* bb = m_vecBackBuffers[i];
			WorkItem(bb->GetBFDC());
			m_aiItemsProcessed++;
		}
		//Else just continue back to the beginning of the while loop.
		else
		{
			continue;
		}
	}
}

std::atomic_int&
ThreadPool::getItemsProcessed()
{
	return m_aiItemsProcessed;
}

