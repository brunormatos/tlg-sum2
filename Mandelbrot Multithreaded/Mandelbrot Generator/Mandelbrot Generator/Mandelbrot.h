/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: Mandelbrot.h
Description	: a class prototype for drawing Mandelbrot fractals
Author		: Bruno Matos (based on template from lecturer Asma Shakil)
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#pragma once

#ifndef __MANDELBROT_H__
#define __MANDELBROT_H__

#include <Windows.h>
#include "backbuffer.h"


/***********************
* tagComplexNumber: a struct for writting complex numbers
* @author: Asma Shakil
********************/
typedef struct tagComplexNumber
{
	float _fReal;
	float _fImag;
} ComplexNumber;

class CMandelBrot
{
public:
	~CMandelBrot();

	/***********************
	* GetInstance(): static member to get the singleton instance of the CMandelBrot class
	* @author: Asma Shakil
	* @arg: no arguments
	* @return: CMandelBrot*
	********************/
	static CMandelBrot* GetInstance();

	/***********************
	* SetMaxIterations: set the maximum number of iterations to compute the fractal (fractal definition degree)
	* @author: Bruno Matos
	* @arg: int		| the new value
	* @return: void
	********************/
	void SetMaxIterations(int _value) { m_iMaxIterations = _value; };

	/***********************
	* SetColorLimit: sets the color RGB limits for the mandelbrot
	* @author: Bruno Matos
	* @arg: int		| the red amount limit
	* @arg: int		| the green amount limit
	* @arg: int		| the blue amount limit
	* @return: void
	********************/
	void SetColorLimit(int _red, int _green, int _blue)
	{
		m_iColorRedLimit = _red;
		m_iColorGreenLimit = _green;
		m_iColorBlueLimit = _blue;
	};

	/***********************
	* SetInnerColor: sets the inner color RGB for the mandelbrot
	* @author: Bruno Matos
	* @arg: int		| the red amount
	* @arg: int		| the green amount
	* @arg: int		| the blue amount
	* @return: void
	********************/
	void SetInnerColor(int _red, int _green, int _blue)
	{
		m_iBgColorRed = _red;
		m_iBgColorGreen = _green;
		m_iBgColorBlue = _blue;
	};
	
	/***********************
	* Draw: computes the complete mandelbrot and draws it into the device context of a target window handler
	* @author: Bruno Matos
	* @arg: HWND		| the target window handler
	* @return: void
	********************/
	void Draw(HWND);

	/***********************
	* Draw: computes the complete mandelbrot and draws it into the device context of a target backbuffer
	* @author: Bruno Matos
	* @arg: CBackBuffer*		| the target backbuffer
	* @return: void
	********************/
	void Draw(CBackBuffer*);

	/***********************
	* Draw: computes a region of the mandelbrot and draws that region into the device context of a target backbuffer
	* @author: Bruno Matos
	* @arg: CBackBuffer*		| the target backbuffer
	* @arg: int		| the region's X starting pixel coordinate
	* @arg: int		| the region's Y starting pixel coordinate
	* @arg: int		| the region's width pixel dimension
	* @arg: int		| the region's height pixel dimension
	* @return: void
	********************/
	void Draw(CBackBuffer*, int, int, int, int);

	/***********************
	* Draw: computes a region of the mandelbrot and draws that region into a target device context
	* @author: Bruno Matos
	* @arg: HDC		| the target device context
	* @arg: int		| the region's X starting pixel coordinate
	* @arg: int		| the region's Y starting pixel coordinate
	* @arg: int		| the region's width pixel dimension
	* @arg: int		| the region's height pixel dimension
	* @arg: int		| the fractal's X scale factor
	* @arg: int		| the fractal's Y scale factor
	* @return: void
	********************/
	void Draw(HDC, int, int, int, int, int, int);
private:
	CMandelBrot();

	// Singleton Instance
	static CMandelBrot* s_pMandelBrotInstance;
	//The current max number of iterations
	int m_iMaxIterations = 10;

	//color limits
	int m_iColorRedLimit = 256;
	int m_iColorGreenLimit = 256;
	int m_iColorBlueLimit = 256;

	int m_iBgColorRed = 0;
	int m_iBgColorGreen = 0;
	int m_iBgColorBlue = 0;
};

#endif