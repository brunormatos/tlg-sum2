/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: main.cpp
Description	: A multi-threaded Mandelbrot Generator application
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/

#include <Windows.h>
#include <vector>
#include <string>
#include <vector>

#include "Mandelbrot.h"		// mandelbrot generator singleton
#include "ThreadPool.h"		// threadpool singleton
#include "clock.h"			// class for measuring time

#include <vld.h>	//uncomment this if wanting to use visual leak detector

//Window properties
#define WINDOW_CLASS_NAME L"Mandelbrot Generator"	//window and class name
const unsigned int _kuiWINDOWWIDTH = 1400;			//window starting width
const unsigned int _kuiWINDOWHEIGHT = 1020;			//window starting height
#define CONFIG_FILE L"config.ini"					//the config file name (including extension)
#define MAX_PATH 256								//maximum char number for directory path
#define MAX_STR_LEN 256								//maximum char number allowed for configuration file keynames

//Global Variables
HINSTANCE g_hInstance;
CClock* g_cclock = nullptr;							//the clock object for measuring time
int g_iBufferCount = 0;								//the total number of back-buffers
int g_iCurrentBB = -1;								//the index of the current back-buffer being displayed (-1 is the main back-buffer)
int g_iMandelbrotMaxIterations = 0;					//the max number of iterations for computing the mandelbrot (degree of definition)
int g_iFractalWidth = 0;							//measurement for scaling the fractal horizontally
int g_iFractalHeight = 0;							//measurement for scaling the fractal vertically
int g_iTotalRegionCount = 0;						//the total number of regions on which the drawing area will be divided								


/***********************
* void GetDesktopResolution: gets the horizontal a vertical dimensions of the desktop area
* @author: Bruno Matos
* @arg: int&		| where the horizontal dimension in pixels will be saved
* @arg: int&		| where the vertical dimension in pixels will be saved
* @return: void
********************/
void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// get a handle to the desktop windows
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	// Top left corner has coordinates (0,0)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}


/***********************
* void GetIntFromConfigIni: read the value of a property saved in the external configuration file
* @author: Bruno Matos
* @arg: LPCWSTR		| the name of the property exactly as it written in the configuration file
* @arg: int			| the default value to be used in case there is an error reading the property value
* @arg: int&		| where the property value will be saved 
* @return: void
********************/
void GetIntFromConfigIni(LPCWSTR _lpKeyName, int _default, int& _destInt)
{
	TCHAR directoryPath[MAX_PATH];
	char readParameter[MAX_STR_LEN];

	GetCurrentDirectory(MAX_STR_LEN, directoryPath);

	std::wstring directoryPathAsWCString(directoryPath);
	directoryPathAsWCString = directoryPathAsWCString + L"\\" + CONFIG_FILE;
	_destInt = GetPrivateProfileInt(L"config", _lpKeyName, _default, directoryPathAsWCString.c_str());
}


LRESULT CALLBACK WindowProc(HWND _hwnd, UINT _uiMsg, WPARAM _wparam, LPARAM _lparam)
{
	PAINTSTRUCT ps;
	static HDC _hWindowDC;	//this must be static	

	// 0. create/get the thread-pool instance
	ThreadPool& threadPool = ThreadPool::GetInstance();			// the singleton will create it

	switch (_uiMsg)
	{
	// when the window is created
	case WM_CREATE:
	{
		// 1. read the variable values from the config file
		int windowWidth = _kuiWINDOWWIDTH, windowHeight = _kuiWINDOWHEIGHT;
		GetDesktopResolution(windowWidth, windowHeight);	// get and save the current desktop resolution

		// from the config file read and save the properties values
		GetIntFromConfigIni(L"threadCount", 4, g_iBufferCount);
		GetIntFromConfigIni(L"mandelbrotMaxIterations", 20, g_iMandelbrotMaxIterations);
		GetIntFromConfigIni(L"fractalWidth", 500, g_iFractalWidth);
		GetIntFromConfigIni(L"fractalHeight", 500, g_iFractalHeight);
		int regionCountX = 0;
		int regionCountY = 0;
		GetIntFromConfigIni(L"regionCountX", 10, regionCountX);
		GetIntFromConfigIni(L"regionCountY", 10, regionCountY);
		// with the correct values for the number of rows and columns of regions we can find out
		//		the total of regions to be drawn
		g_iTotalRegionCount = regionCountX * regionCountY;
		// and then compute the width and height of a region, based on
		int regionWidth = static_cast<int>(
			std::floor(
				static_cast<float>(windowWidth) / 
				static_cast<float>(regionCountX)));					// the desktop width and the number of columns
		int regionHeight = static_cast<int>(
			std::floor(
				static_cast<float>(windowHeight) / 
				static_cast<float>(regionCountY)));					// the desktop height and the number of rows


		// 2. create the mandelbrot and set the number of iterations from the config file
		int mbColorLimitRed = 0;
		int mbColorLimitGreen = 0;
		int mbColorLimitBlue = 0;
		int mbColorBgRed = 0;
		int mbColorBgGreen = 0;
		int mbColorBgBlue = 0;
		GetIntFromConfigIni(L"mandelbrotColorLimitRed", 256, mbColorLimitRed);
		GetIntFromConfigIni(L"mandelbrotColorLimitGreen", 256, mbColorLimitGreen);
		GetIntFromConfigIni(L"mandelbrotColorLimitBlue", 186, mbColorLimitBlue);
		GetIntFromConfigIni(L"mandelbrotColorBgRed", 256, mbColorBgRed);
		GetIntFromConfigIni(L"mandelbrotColorBgGreen", 256, mbColorBgGreen);
		GetIntFromConfigIni(L"mandelbrotColorBgBlue", 256, mbColorBgBlue);
		CMandelBrot* mandelbrot = CMandelBrot::GetInstance();		// the singleton will create it
		mandelbrot->SetMaxIterations(g_iMandelbrotMaxIterations);
		mandelbrot->SetColorLimit(mbColorLimitRed, mbColorLimitGreen, mbColorLimitBlue);
		mandelbrot->SetInnerColor(mbColorBgRed, mbColorBgGreen, mbColorBgBlue);

		// 3. the number of threads available equal to the number of back-buffers (read from config file
		threadPool.SetThreadNumber(g_iBufferCount);					// setting the number of threads available equal to the number of back-buffers

		// 4. initialize main back-buffer created by the thread-pool when the thread-pool was created
		threadPool.m_pMainBackBuffer->Initialise(_hwnd, windowWidth, windowHeight);

		// 5. create and initialize a collection of auxiliary back-buffers
		for (unsigned int i = 0; i < g_iBufferCount; i++) {					// matching the same number of threads available
			CBackBuffer* backbuffer = new CBackBuffer();
			backbuffer->Initialise(_hwnd, windowWidth, windowHeight);		// they should occupy the full desktop
			threadPool.m_vecBackBuffers.push_back(backbuffer);				// and adding each into the thread-pool vector also collecting the 
		}

 		threadPool.Initialize();
 		threadPool.Start();

		// 6. submit the tasks for the threadpool to perform
		for (int j = 0; j < regionCountY; j++)
		{
			for (int i = 0; i < regionCountX; i++)
			{
				threadPool.Submit(CTask(regionWidth*i, regionHeight * j, regionWidth, regionHeight, g_iFractalWidth, g_iFractalHeight));
			}
		}		
	}
	break;
	case WM_KEYDOWN:
	{
		switch (_wparam)
		{
		case VK_TAB:	//cycles through the available backbuffers for displaying each backbuffer device context
			if (g_iCurrentBB == -1)
				threadPool.m_pMainBackBuffer->Present();
			else
				threadPool.m_vecBackBuffers[g_iCurrentBB]->Present();
			++g_iCurrentBB;
			if (g_iCurrentBB == g_iBufferCount) g_iCurrentBB = -1;
			break;
		case VK_RETURN:		//presents the contents of the main back-buffer which has the complete drawing
			threadPool.m_pMainBackBuffer->Present();
			g_iCurrentBB = -1;
			break;
		case VK_ESCAPE:
		{
			SendMessage(_hwnd, WM_CLOSE, 0, 0);
			return(0);
		}
		break;
		default:
			break;
		}
	}
	break;
	case WM_PAINT:		// will drawing the contents of the current back-buffer every time the window is resized
	{
		if (g_iCurrentBB == -1)
			threadPool.m_pMainBackBuffer->Present();
		else
			threadPool.m_vecBackBuffers[g_iCurrentBB]->Present();
		return (0);
	}
	break;	
	case WM_CLOSE:
	{
		PostQuitMessage(0);
	}
	break;
	default:
		break;
	}
	return (DefWindowProc(_hwnd, _uiMsg, _wparam, _lparam));
}


HWND CreateAndRegisterWindow(HINSTANCE _hInstance)
{
	WNDCLASSEX winclass; // This will hold the class we create.
	HWND hwnd;           // Generic window handle.

						 // First fill in the window class structure.
	winclass.cbSize = sizeof(WNDCLASSEX);
	winclass.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc = WindowProc;
	winclass.cbClsExtra = 0;
	winclass.cbWndExtra = 0;
	winclass.hInstance = _hInstance;
	winclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	winclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winclass.hbrBackground =
		static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	winclass.lpszMenuName = NULL;
	winclass.lpszClassName = WINDOW_CLASS_NAME;
	winclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register the window class
	if (!RegisterClassEx(&winclass))
	{
		return (0);
	}

	// create the window
	hwnd = CreateWindowEx(NULL, // Extended style.
		WINDOW_CLASS_NAME,      // Class.
		WINDOW_CLASS_NAME,		// Title.
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPSIBLINGS ,
		10, 10,                  // Initial x,y.
		_kuiWINDOWWIDTH, _kuiWINDOWHEIGHT,                // Initial width, height.
		NULL,                   // Handle to parent.
		NULL,                   // Handle to menu.
		_hInstance,             // Instance of this application.
		NULL);                  // Extra creation parameters.

	return hwnd;
}



int WINAPI WinMain(HINSTANCE _hInstance,
	HINSTANCE _hPrevInstance,
	LPSTR _lpCmdLine,
	int _nCmdShow)
{
	MSG msg;  //Generic Message

	g_cclock = new CClock();
	g_cclock->Initialise();
	g_cclock->Process();
	ThreadPool& threadPool = ThreadPool::GetInstance();			// initiate threadPool
	threadPool.m_pClock = g_cclock;								// share with it the reference for the global clock

	HWND _hwnd = CreateAndRegisterWindow(_hInstance);
	if (!(_hwnd))
	{
		return (0);
	}

	CMandelBrot* mandelbrot = CMandelBrot::GetInstance();		

	// Enter main event loop
	while (true)
	{
		// this is to stop the thread pool after all items have been processed
		if (threadPool.getItemsProcessed() == g_iTotalRegionCount && threadPool.bIsRunning())
		{
			threadPool.Stop();
		}

		// Test if there is a message in queue, if so get it.
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// Test if this is a quit.
			if (msg.message == WM_QUIT)
			{
				break;
			}
			// Translate any accelerator keys.
			TranslateMessage(&msg);
			// Send the message to the window proc.
			DispatchMessage(&msg);
		}

	}
	
	delete g_cclock;
	delete mandelbrot;
	threadPool.DestroyInstance();
	// Return to Windows like this...
	return (static_cast<int>(msg.wParam));
}
