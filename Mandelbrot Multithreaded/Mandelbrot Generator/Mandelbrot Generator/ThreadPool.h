/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: ThreadPool.h
Description	: Thread pool class header file
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#pragma once

#ifndef __THREADPOOL_H__
#define __THREADPOOL_H__

#include <vector>
#include <thread>
#include <atomic>
#include <functional>
#include <condition_variable>

#include "WorkQueue.h"
#include "Task.h"
#include "backbuffer.h"
#include "clock.h"

class ThreadPool
{
public:
	~ThreadPool();

	// Singleton Methods
	/***********************
	* GetInstance: static method for getting the singleton instance of the threadpool
	* @author: Bruno Matos
	* @arg: no arguments
	* @return: ThreadPool&
	********************/
	static ThreadPool& GetInstance();

	/***********************
	* DestroyInstance: static method for destroying the singleton instance of this class
	* @author: Bruno Matos
	* @return: void
	********************/
	static void DestroyInstance();


	/***********************
	* Initialize: creates a working queue for receiving quotes
	* @author: Bruno Matos
	* @arg: no arguments
	* @return: void
	********************/
	void Initialize();

	/***********************
	* Submit: adds a task to the work queue for being dispatched by the thread pool to one of the threads
	* @author: Bruno Matos
	* @arg: CTask		| item/task to be processed
	* @return: void
	********************/
	void Submit(CTask _iworkItem);


	/***********************
	* DoWork: the entry point of each thread; attempts to pop an element from work queue and process it
	* @author: Bruno Matos
	* @arg: int		| the index of the thread responsible for processing thhe current task
	* @return: void
	********************/
	void DoWork(unsigned int);

	/***********************
	* Start: starts the multithreading
	* @author: Bruno Matos
	* @arg: no arguments
	* @return: void
	********************/
	void Start();

	/***********************
	* Stop: stops the multi-threading 
	* @author: Bruno Matos
	* @arg: no arguments
	* @return: void
	********************/
	void Stop();

	/***********************
	* getItemsProcessed: get the number of processed items through the threadpool
	* @author: Bruno Matos
	* @arg: no arguments
	* @return: std::atomic_int&
	********************/
	std::atomic_int& getItemsProcessed();

	/***********************
	* GetThreadCount : returns the number of threads available to the threadpool
	* @author: Bruno Matos
	* @return: unsigned int& 
	********************/
	unsigned int& GetThreadCount() { return m_iNumberOfThreads; };

	/***********************
	* SetThreadNumber(const unsigned int): sets a new number to the number of threads
	* @author: Bruno Matos
	* @arg: const unsigned int		| the new number of threads
	* @return: void
	********************/
	void SetThreadNumber(const unsigned int _value) { m_iNumberOfThreads = _value; };

	//A collection of pointers of backbuffers to draw on and display
	std::vector<CBackBuffer*> m_vecBackBuffers;

	//The main backbuffer that will contain the complete drawing
	CBackBuffer* m_pMainBackBuffer = nullptr;

	//An atomic boolean variable to know if the threadpool is running or not
	std::atomic_bool bIsRunning() { return !m_bStop; };

	//A reference to an instance of the type CClock for measuring loading time
	CClock* m_pClock = nullptr;

private:
	
	/***********************
	* ThreadPool(): the default constructor (not public as this is a singleton);
					sets the number of threads to hardware_concurrency and creates the mainBackBuffer object
	* @author: Bruno Matos
	* @return: ThreadPool
	********************/
	ThreadPool();

	//The ThreadPool is non-copyable.
	ThreadPool(const ThreadPool& _kr) = delete;
	ThreadPool& operator= (const ThreadPool& _kr) = delete;

protected:
	static ThreadPool* s_pThreadPool;

private:
	//An atomic boolean variable to stop all threads in the threadpool
	std::atomic_bool m_bStop{ false };

	//A WorkQueue of tasks which are functors
	CWorkQueue<CTask >* m_pWorkQueue;

	//Create a pool of worker threads
	std::vector<std::thread> m_workerThreads;

	//A variable to hold the number of threads we want in the pool
	unsigned int m_iNumberOfThreads;

	//An atomic variable to keep track of how many items have been processed.
	std::atomic_int m_aiItemsProcessed{ 0 };
};
#endif
