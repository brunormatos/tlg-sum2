#include <iostream>
#include <fstream>
#include <string>
//#include <vld.h>

#include "ThreadPool.h"
#include "WorkQueue.h"
#include "Task.h"

int main()
{
	// LOAD number of available threads from configuration file
	std::string STRING;
	std::ifstream infile;
	infile.open("config.txt");
	while (!infile.eof()) // Gets all lines inside file.
	{
		std::getline(infile, STRING); // Saves the line in STRING.
		//std::cout << STRING; // Prints our STRING.
	}
	infile.close();
	const int totalThreadCount = std::stoi (STRING.substr(STRING.find("=") + 1));
	std::cout << totalThreadCount << " threads" << std::endl;

	srand((unsigned int)time(0));
	const int kiTOTALITEMS = 20;
	//Create a ThreadPool Object capable of holding as many threads as the number of cores
	ThreadPool& threadPool = ThreadPool::GetInstance(totalThreadCount);
	//Initialize the pool
	threadPool.Initialize();
	threadPool.Start();
	// The main thread writes items to the WorkQueue
	for (int i = 0; i < kiTOTALITEMS; i++)
	{
		threadPool.Submit(CTask(i));
		std::cout << "Main Thread wrote item " << i << " to the Work Queue " << std::endl;
		//Sleep for some random time to simulate delay in arrival of work items
		std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 1001));
	}
	if (threadPool.getItemsProcessed() == kiTOTALITEMS)
	{
		threadPool.Stop();
	}

	threadPool.DestroyInstance();

	int iTemp;
	std::cin >> iTemp;

	return 0;
}